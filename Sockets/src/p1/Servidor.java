package p1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Servidor {
	static final int PUERTO=5000;
	private enum Move {
        PIEDRA, PAPEL, TIJERAS;	
		 public int compareMoves(Move otherMove) {
	            // Empate
	            if (this == otherMove)
	                return 0;
	 
	            switch (this) {
	            case PIEDRA:
	                return (otherMove == TIJERAS ? 1 : -1);
	            case PAPEL:
	                return (otherMove == PIEDRA ? 1 : -1);
	            case TIJERAS:
	                return (otherMove == PAPEL ? 1 : -1);
	            }
	 
	            // Nunca debería llegar hasta aquí
	            return 0;
	        }
	}
	public Servidor( ) {
	try {
		ServerSocket skServidor = new ServerSocket(PUERTO);
		System.out.println("Joc llest ... esperant client");
		Socket skclient;
		int usuariopuntos=0,computadorapuntos=0;
		
		while(true) {
			skclient = skServidor.accept();
			Move computadora = getMove();
			DataInputStream flujo = new DataInputStream(skclient.getInputStream());
			Move playerchoose = Move.valueOf(flujo.readUTF());
			DataOutputStream flujo2= new DataOutputStream(skclient.getOutputStream());
			int compareMoves = playerchoose.compareMoves(computadora);
		    switch (compareMoves) {
		       case 0: // Empate
		           flujo2.writeUTF("Empate!");
		           break;
		       case 1: // Gana Usuario
		    	   flujo2.writeUTF(playerchoose + " le gana a " + computadora + ". Ganaste!");
		    	   usuariopuntos++;
		           flujo2.writeUTF("u");
		           break;
		       case -1: // Gana Computadora
		    	   flujo2.writeUTF(computadora + " le gana a " + playerchoose + ". Perdiste.");
		    	   flujo2.writeUTF("c");
		           break;
		    }
		    skclient.close();   
			
		}
	} catch( Exception e ) {
		System.out.println( e.getMessage() );
	}
	}
	public static void main( String[] arg ) {
		new Servidor();
	}
	public Move getMove() {
        Move[] moves = Move.values();
        Random random = new Random();
        int index = random.nextInt(moves.length);
        return moves[index];
    }
}
