package p1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Cliente {

	static final String HOST = "172.31.63.22";
	static final int PUERTO = 5000;
	int usuarioPuntos = 0, computadoraPuntos = 0, cantidadDeJuegos;

	private enum Move {
		PIEDRA, PAPEL, TIJERAS;
	}

	public Cliente() throws UnknownHostException, IOException {

		while (true) {
			Socket skclient = new Socket(HOST, PUERTO);
			System.out.println("Piedra, papel o tijeras? ");
			// Obtener la entrada del usuario
			Move playerchoose = getMove();
			DataOutputStream flujo2 = new DataOutputStream(skclient.getOutputStream());
			flujo2.writeUTF(playerchoose.name());
			DataInputStream flujo = new DataInputStream(skclient.getInputStream());
			String result = flujo.readUTF();
			System.out.println(result);
			if (!result.equals("Empate!")) {
				String st = flujo.readUTF();
				if (st.equals("u"))
					usuarioPuntos++;
				else
					computadoraPuntos++;
			}
			cantidadDeJuegos++;
			skclient.close();
			printGameStats();
			if ((usuarioPuntos == 3) || (computadoraPuntos == 3))
				if (usuarioPuntos == 3)
					System.out.println("You win");
				else
					System.out.println("You Lose");
			break;
		}

	}

	private Move getMove() {
		// Solicitarle al usuario
		System.out.println("1. Piedra\n2. Papel\n3.tijeras ");
		Scanner in = new Scanner(System.in);
		// Obtener la entrada del usuario
		int opt = 0;
		try {
			opt = in.nextInt();
		} catch (Exception e) {
			System.out.println("Tienes que escoger una opción válida.");
			getMove();
		}
		switch (opt) {
		case 1:
			return Move.PIEDRA;
		case 2:
			return Move.PAPEL;
		case 3:
			return Move.TIJERAS;
		default:
			System.out.println("Tienes que escoger una opción válida.");
			getMove();
		}
		return getMove();
	}

	private void printGameStats() {
		int wins = usuarioPuntos;
		int losses = computadoraPuntos;
		int ties = cantidadDeJuegos - usuarioPuntos - computadoraPuntos;
		// Línea
		System.out.print("+");
		printDashes(49);
		System.out.println("+");
		// Imprimir títulos
		System.out.printf("|  %6s  |  %6s  |  %6s  |  %6s  |\n", "VICTORIAS", "DERROTAS", "EMPATES", "JUEGOS");
		// Línea
		System.out.print("|");
		printDashes(13);
		System.out.print("+");
		printDashes(12);
		System.out.print("+");
		printDashes(11);
		System.out.print("+");
		printDashes(10);
		System.out.println("|");
		// Imprimir valores
		System.out.printf("|  %9d  |  %8d  |  %7d  |  %6d  |\n", wins, losses, ties, cantidadDeJuegos);
		// Línea
		System.out.print("+");
		printDashes(49);
		System.out.println("+");
	}

	private void printDashes(int numberOfDashes) {
		for (int i = 0; i < numberOfDashes; i++) {
			System.out.print("-");
		}
	}

	public static void main(String[] arg) throws UnknownHostException, IOException {
		new Cliente();
	}
}
