package p2_1;

import java.io.*;
import java.net.*;
class ServidorTCP
{
	public static void main(String args[])
	{
		// Primero indicamos la dirección IP local
		try
		{
			System.out.println("LocalHost = " +
					InetAddress.getLocalHost().toString());
		}
		catch (UnknownHostException uhe)
		{
			System.err.println("No puedo saber la dirección IP local : " + uhe);
		}
		// Abrimos un "Socket de Servidor" TCP en el puerto 1234.
		ServerSocket ss = null;
		try
		{
			ss = new ServerSocket(1234);
		}
		catch (IOException ioe)
		{
			System.err.println("Error al abrir el socket de servidor : " + ioe);
			System.exit(-1);
		}
		int entrada;
		long salida;
		// Bucle infinito
		while(true)
		{
			try
			{
				// Esperamos a que alguien se conecte a nuestro Socket
				Socket sckt = ss.accept();
				HiloServidorTCP hilo = new HiloServidorTCP(sckt);
				hilo.start();
			
			}
			catch(Exception e)
			{
				System.err.println("Se ha producido la excepción : " +e);
			}
		}
	}
}
