package p2_1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class HiloServidorTCP extends Thread{
	Socket sckt;
	public HiloServidorTCP(Socket sckt) {
		this.sckt=sckt;
	}
	public void run() {
		// Extraemos los Streams de entrada y de salida
		DataInputStream dis;
		try {
			dis = new
					DataInputStream(sckt.getInputStream());
 
			DataOutputStream dos = new
					DataOutputStream(sckt.getOutputStream());
			// Podemos extraer información del socket
			// No de puerto remoto
			int puerto = sckt.getPort();
			// Dirección de Internet remota
			InetAddress direcc = sckt.getInetAddress();
			// Leemos datos de la peticion
			int entrada = dis.readInt();
			// Calculamos resultado
			long salida = (long)entrada*(long)entrada;
			// Escribimos el resultado
			dos.writeLong(salida);
			// Cerramos los streams
			dis.close();
			dos.close();
			sckt.close();
			// Registramos en salida estandard
			System.out.println( "Cliente = " + direcc + ":"+ puerto+ "\tEntrada = " + entrada + "\tSalida = "  +salida );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
